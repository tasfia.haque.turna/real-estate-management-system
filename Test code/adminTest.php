<?php

class adminTest extends \PHPUnit\Framework\TestCase
{
	//protected function getadminModel(){}

	protected $admin;

	public function setFirstName(): void
 {   
    parent::setFirstName();
    var_dump('1');
 }
 public function setLastName(): void
 {   
    parent::setLastName();
    var_dump('1');
 }
 

	public function GetTheFirstName()
	{
        parent::setFirstName();
		$this->admin->setFirstName('Min');

		$this->assertEquals($this->admin->getFirstName(), 'Min');
	}

	public function GetTheLastName()
	{
		$this->admin->setLastName('Yoongi');

		$this->assertEquals($this->admin->getLastName(), 'Yoongi');
	}

	public function testFullName()
	{
		$admin = new \App\Models\Admin;

		$admin->setFirstName('Min');

		$admin->setLastName('Yoongi');

		$this->assertEquals($admin->getFullName(), 'Min Yoongi');
	}

	public function FirstAndLastNameAreTrimmed()
	{
		$admin = new \App\Models\Admin;

		$admin->setFirstName('Min     ');

		$admin->setLastName('     Yoongi');

		$this->assertEquals($admin->getFirstName(), 'Min');

		$this->assertEquals($admin->getLastName(), 'Yoongi');
	}


	public function testEmailAddressCanBeSet()
	{
		$admin = new \App\Models\Admin;

		$admin->setEmail('myg93@gmail.com');

		$this->assertEquals($admin->getEmail(), 'myg93@gmail.com');
	}

	public function testEmailVariablesContainCorrectValues()
	{
		$admin = new \App\Models\Admin;

		$admin->setFirstName('Min');

		$admin->setLastName('Yoongi');

		$admin->setEmail('myg93@gmail.com');

		$emailVariables = $admin->getEmailVariables();

		$this->assertArrayHasKey('full_name', $emailVariables);
		$this->assertArrayHasKey('email', $emailVariables);

		$this->assertEquals($emailVariables['full_name'], 'Min Yoongi');
		$this->assertEquals($emailVariables['email'], 'myg93@gmail.com');

		
	}
}